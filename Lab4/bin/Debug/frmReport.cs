﻿using lab04.Model;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab04
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }
        private void frmReport_Load(object sender, EventArgs e)
        {
            StudentContextDB db = new StudentContextDB();
            var listStudentReportDto = db.Students.Select(c => new StudentReport
            {
                StudentID = c.StudentID,
                FullName = c.FullName,
                Averagescore = c.Averagescore,
                FacultyID = c.Faculty.FacultyID,

            }).ToList();
            this.reportViewer1.LocalReport.ReportPath = "rptStudentReport.rdlc";
            var reportDataSource = new ReportDataSource("StudentReportDataSet", listStudentReportDto);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();

        }
    }

    }
